from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed, APIException
from rest_framework import status
from django.utils.translation import gettext_lazy as _
import os


class KeyNotFound(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('API-KEY header is needed.')
    default_code = 'key_not_found'


class InvalidKey(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('API-KEY is invalid.')
    default_code = 'key_invalid'


class APIAuthentication(BaseAuthentication):
    def authenticate(self, request):
        key = request.headers.get('API-KEY', None)
        if not key:
            raise KeyNotFound

        if key != os.environ.get('API_KEY'):
            raise InvalidKey
