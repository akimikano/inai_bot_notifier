from django.utils.deprecation import MiddlewareMixin
from django.core.exceptions import PermissionDenied


class ServiceMiddleware(MiddlewareMixin):

    def process_request(self, request):
        key = request.headers.get('API-KEY', None)
        if not key:
            raise PermissionDenied('API-KEY header is needed.')

        if key != 'test':
            raise PermissionDenied('API-KEY is invalid.')


