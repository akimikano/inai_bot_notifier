from rest_framework import serializers


class SendSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(required=True)
    title = serializers.CharField(required=True)
    body = serializers.CharField(required=True)


class SendAllSerializer(serializers.Serializer):
    title = serializers.CharField(required=True)
    body = serializers.CharField(required=True)
