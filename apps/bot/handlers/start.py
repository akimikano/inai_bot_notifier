from apps.bot.init_bot import bot
from telebot.types import Message, CallbackQuery
from apps.bot.utils import code
import pathlib as pl


@bot.message_handler(commands=['start'])
def start_handler(message: Message, data: dict):
    bot.delete_state(message.from_user.id, message.chat.id)
    try:
        getattr(message, 'is_created')
        if message.is_created:
            text = 'Добро пожаловать в Электронную библиотеку! Вы успешно подключились к боту!'
        else:
            text = 'Я буду отправлять вам уведомления!'
    except AttributeError:
        text = 'Вам необходимо подключиться по ссылке с сайта.'
    bot.send_message(message.chat.id, text)
