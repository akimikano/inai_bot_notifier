import logging
import telebot
from telebot import StateMemoryStorage
from apps.bot.middlewares import *
import os
# from dotenv import load_dotenv
import flask
from apps.bot import utils
from apps.bot.middlewares.auth import AuthMiddleware
from loguru import logger

state_storage = StateMemoryStorage()
TOKEN = '5722231554:AAGPZXm1a79bleYrFFPiVWDnKYXceCGthhc'
# TOKEN = '5796444658:AAE4B92EFgaGJi8P2OXLMNEGPvIIBSgVXoU'

HOST = 'http://67.207.94.104:8010'

bot = utils.AnswerBot(TOKEN,
                      use_class_middlewares=True,
                      parse_mode='HTML',
                      state_storage=state_storage)

telebot.logger.setLevel(logging.DEBUG)
telebot.apihelper.ENABLE_MIDDLEWARE = True

bot.setup_middleware(AuthMiddleware())

WEBHOOK_HOST = os.environ.get('WEBHOOK_HOST')
WEBHOOK_PORT = os.environ.get('WEBHOOK_PORT')
WEBHOOK_LISTEN = os.environ.get('WEBHOOK_LISTEN')

WEBHOOK_URL_PATH = "/botwebhook"


def run_bot_prod(bot):
    app = flask.Flask(__name__)

    @app.route('/', methods=['GET', 'HEAD'])
    def index():
        return ''

    # Process webhook calls
    @app.route(WEBHOOK_URL_PATH, methods=['POST'])
    def webhook():
        logger.debug(flask.request)
        if flask.request.headers.get('content-type') == 'application/json':
            json_string = flask.request.get_data().decode('utf-8')
            update = telebot.types.Update.de_json(json_string)
            bot.process_new_updates([update])
            return ''
        else:
            flask.abort(403)

    bot.remove_webhook()

    bot.set_webhook(url=HOST + WEBHOOK_URL_PATH)

    app.run(host='0.0.0.0',
            port=8000,
            debug=False)


