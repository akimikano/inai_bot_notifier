from apps.bot import views
from django.urls import path, include


urlpatterns = [
    path('bot_notify/', views.SendView.as_view()),
    path('bot_notify_all/', views.SendAllView.as_view())
]