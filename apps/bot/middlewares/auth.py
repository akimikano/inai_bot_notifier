from telebot import BaseMiddleware
from telebot.types import (
    Message, CallbackQuery
)
from telebot.util import update_types
from apps.bot.models import UserBot
from typing import Union
from loguru import logger


class AuthMiddleware(BaseMiddleware):
    def __init__(self):
        super(AuthMiddleware, self).__init__()
        self.update_sensitive = False
        self.update_types = ['message', 'callback_query']

    def pre_process(self, message: Union[Message, CallbackQuery], data):
        if isinstance(message, Message):
            logger.debug(message.text)
            if '/start' in message.text:

                text = message.text.split()
                if len(text) == 2:
                    user_id = text[1]
                    user_data = message.from_user.to_dict()
                    user_data['user_id'] = int(user_id)
                    logger.debug(user_id)
                    logger.debug(user_data)
                    message.u, message.is_created = UserBot.get_user_and_created(user_data)
                    logger.debug(message.u)
                    data['user'] = message.u

    def post_process(self, message: Message, data, exception):
        pass
