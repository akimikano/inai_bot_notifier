from rest_framework import viewsets, views, mixins
from apps.bot.serializers import *
from apps.bot.init_bot import bot
from apps.bot.models import UserBot
from rest_framework.exceptions import APIException
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response


class SendView(views.APIView):
    def post(self, request):
        serializer = SendSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user_bot = UserBot.objects.get(user_id=serializer.data.get('user_id'))
        except ObjectDoesNotExist:
            raise APIException('Bot user does not exist.', 400)

        title = serializer.data.get('title')
        body = serializer.data.get('body')
        message = f'{title}\n\n{body}'
        bot.send_message(user_bot.id, message)
        return Response({'success': True})


class SendAllView(views.APIView):
    def post(self, request):
        serializer = SendAllSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        title = serializer.data.get('title')
        body = serializer.data.get('body')
        message = f'{title}\n\n{body}'
        user_bots = UserBot.objects.all()
        for user_bot in user_bots:
            try:
                bot.send_message(user_bot.id, message)
            except:
                continue
        return Response({'success': True})
