from typing import Union, Optional
from django.db import models
from django.forms import model_to_dict
import json
from loguru import logger


class UserBot(models.Model):
    class Meta:
        verbose_name = 'Пользователь телеграма'
        verbose_name_plural = 'Пользователи телеграма'

    id = models.IntegerField('Telegram ID', primary_key=True)
    user_id = models.PositiveBigIntegerField('User ID', blank=True, null=True)
    username = models.CharField('Имя пользователя', max_length=32, null=True)
    first_name = models.CharField('Имя', max_length=100, null=True)
    last_name = models.CharField('Фамилия', max_length=100, null=True)
    language_code = models.CharField('Язык', max_length=8, null=True)
    deep_link = models.CharField('Диплинк', max_length=64, null=True)
    is_blocked_bot = models.BooleanField('Заблокированный бот', default=False)
    is_bot = models.BooleanField('Бот', default=False)
    is_admin = models.BooleanField('Администратор', default=False)

    @classmethod
    def get_user_and_created(cls, user_data: dict):
        """ python-telegram-bot's Update, Context --> User instance """
        data = cls.init_kwargs(user_data)

        u, created = cls.objects.update_or_create(defaults=data, id=data['id'])

        logger.info(f"User {u.tg_str} created: {created}")
        return u, created

    # @classmethod
    # def get_user(cls, user_data: User) -> UserBot:
    #     u, _ = cls.get_user_and_created(user_data)
    #     return u

    @classmethod
    def init_kwargs(cls, arg_dict):
        model_fields = [f.name for f in cls._meta.get_fields()]
        return {k: v for k, v in arg_dict.items() if k in model_fields}

    @classmethod
    def get_user_by_username_or_user_id(cls, username_or_user_id: Union[str, int]):
        """ Search user in DB, return User or None if not found """
        username = str(username_or_user_id).replace("@", "").strip().lower()
        if username.isdigit():  # user_id
            return cls.objects.filter(user_id=int(username)).first()
        return cls.objects.filter(username__iexact=username).first()

    def to_json(self):
        values = model_to_dict(self)
        data = json.dumps(values)
        return data

    @property
    def tg_str(self) -> str:
        if self.username:
            return f'@{self.username}'
        return f"{self.first_name} {self.last_name or ''}"


